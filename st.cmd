require essioc
require iocmetadata
require fimioc

###############################################################################
# Environmental variables settings
###############################################################################

### EPICS configurations
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, 10000000)
epicsEnvSet(NELM, 8000)
callbackSetQueueSize(10000)

### Naming macros
epicsEnvSet(PREFIX,"MBL-050RFC")
epicsEnvSet(KLY, "4")
epicsEnvSet(IOCDEVICE, "SC-IOC-$(KLY)02")
epicsEnvSet(IOCNAME, "$(PREFIX):$(IOCDEVICE)")
epicsEnvSet(IOCDIR, "$(PREFIX)_$(IOCDEVICE)")

### External file with individual macros for all input channels
epicsEnvSet(FIMDEVICE, "RFS-FIM-$(KLY)01")
iocshLoad("fim_config.iocsh", "P=$(PREFIX):, R=$(FIMDEVICE):")

###############################################################################
# EVR timestamp definition
###############################################################################
epicsEnvSet("TIMESTAMP_SRC", "$(PREFIX):RFS-EVR-$(KLY)01:EvtACnt-I")

###############################################################################
# Calibration files directory definition
###############################################################################
epicsEnvSet("CALIB_FOLDER", "/opt/calibration/")

###############################################################################
# IFC1410 EPICS driver for RFLPS FIM firmware
###############################################################################
ndsCreateDevice(ifc1410_fim, $(PREFIX), card=0, fmc=1, files=$(CALIB_FOLDER=/tmp), winsize=$(NELM), dataformat=0)

###############################################################################
# General records (system)
###############################################################################
dbLoadRecords("$(fimioc_DB)/fimioc.template", "P=$(PREFIX):, R=$(FIMDEVICE):, NDSROOT=$(PREFIX), NDS0=FSM, NELM=$(NELM)")
dbLoadRecords("$(fimioc_DB)/fimioc-timestamp.template", "P=$(PREFIX):, R=$(FIMDEVICE):, TIMESTAMP_SRC=$(TIMESTAMP_SRC), NDSROOT=$(PREFIX), NDS0=FSM")
dbLoadRecords("$(fimioc_DB)/fimioc-ilck-list.template", "P=$(PREFIX):, R=$(FIMDEVICE):")

###############################################################################
# Analog Inputs Records
###############################################################################
iocshLoad("$(fimioc_DIR)/fimLoad_AI.iocsh", "IOCDEVICE=$(FIMDEVICE)")

###############################################################################
# Digital Inputs Records
###############################################################################
iocshLoad("$(fimioc_DIR)/fimLoad_DI.iocsh", "IOCDEVICE=$(FIMDEVICE)")

###############################################################################
# Reflected Power Special Blocks Records
###############################################################################
iocshLoad("$(fimioc_DIR)/fimLoad_special.iocsh", "IOCDEVICE=$(FIMDEVICE)")



###############################################################################
# Sequencer setup - automatic reset of interlocks from SIM inputs
###############################################################################
epicsEnvSet("SEQ_1", "$(PREFIX):$(DEV_DI0):$(CHN_DI0)-isFirstIlck")
epicsEnvSet("SEQ_2", "$(PREFIX):$(DEV_DI0):$(CHN_DI0)-IlckRst")
epicsEnvSet("SEQ_3", "$(PREFIX):$(DEV_DI1):$(CHN_DI1)-isFirstIlck")
epicsEnvSet("SEQ_4", "$(PREFIX):$(DEV_DI1):$(CHN_DI1)-IlckRst")
afterInit('seq fimioc_automation_seq "P=$(PREFIX):, R=$(FIMDEVICE):, HVON_FST=$(SEQ_1), HVON_RST=$(SEQ_2), RFON_FST=$(SEQ_3), RFON_RST=$(SEQ_4)"')

###############################################################################
# Create metadata for save-restore and archiver
###############################################################################
pvlistFromInfo("ARCHIVE_THIS", "$(PREFIX):$(IOCDEVICE):ArchiverList")
pvlistFromInfo("ARCHIVEWAVE_THIS", "$(PREFIX):$(IOCDEVICE):ArchiverWavList")
pvlistFromInfo("SAVRES_THIS", "$(PREFIX):$(IOCDEVICE):SavResList")

###############################################################################
# essioc common records
###############################################################################
# Disable ioclog and caputlog since diskless hosts don't have syslog-ng configured
epicsEnvSet("LOGDISABLE", "1")
iocshLoad("$(essioc_DIR)/common_config.iocsh", "AS_TOP=/opt/nonvolatile")
afterInit("caPutJsonLogReconf -1")

###############################################################################
# Force processing of restored PVs
###############################################################################
afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/values_pass1.sav")")

###############################################################################
# Set archiver infotags for iocStats records
###############################################################################
dbLoadRecords("$(PWD)/db/infotag-archiver.template", "PVNAME=$(IOCNAME):Heartbeat")
dbLoadRecords("$(PWD)/db/infotag-archiver.template", "PVNAME=$(IOCNAME):LOAD")

###############################################################################
# Start IOC
###############################################################################
iocInit()

